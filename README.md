# Stratego
![Stratego](stratego.png)

A command-line of the board game Stratego. 


Compile:
g++ -o stratego *.cc

./stratego [redPlayer Layout] [BluePlayer Layout]



rules can be found here https://en.wikipedia.org/wiki/Stratego#Gameplay
