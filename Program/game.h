#ifndef __GAME_H__
#define __GAME_H__

#include <iostream>
#include <string>
#include <fstream>
#include "piece.h"
#include "player.h"
#include <vector>
#include <map>


class Game{
	
	//bool running;
	public:
		
	Game(char **argv);
	 Player* RedP;
	 Player* BlueP;
	 Player* curPlayer;
	public:
	~Game();
	 Tile* board[10][10];
	 map<int,std::string> pieceNames;
	 int RED = 0;
	 int BLUE = 1;
	 void init(char **argv);
	 Player* getNextPlayer();
	 Player* getOpponent();
	 void displayBoard();
	 void setUpplayer(int color, ifstream& );
	 void menu();
	int gamesPlayed =0;
	 void play();
	 void genNextMove();
	 bool isGameover;
	 bool isMoveValid(int,int,int,int,Player*);

	 void combat(int,int,int,int);
};
#endif
