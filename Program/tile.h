#ifndef __TILE_H__
#define __TILE_H__
class Piece;

class Tile{
	public:
	int rank;
	int file;
	Piece* Occupant;
	bool isWater;

	Tile();
	Tile(int rank, int file);
	~Tile();
	void setPiece(Piece * p);
	bool isOccupied()const;
	Piece* getOccupant()const;
	int getRank()const;
	int getFile()const;
	void display();
};
#endif
