#include <string>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

//#include "piece.h"
#include "game.h"
#include "player.h"

	
void Game :: init(char **argv){
	//setup tiles
	for(int i = 0; i < 10; ++i){
		for(int j = 0; j < 10; ++j){
			board[i][j] = new Tile(i,j);
		}
	}

	pieceNames.insert(std::make_pair(BOMB,"Bomb"));
	pieceNames.insert(std::make_pair(MARSHAL,"Marshal"));
	pieceNames.insert(std::make_pair(GENERAL,"General"));
	pieceNames.insert(std::make_pair(COLONEL,"Colonel"));
	pieceNames.insert(std::make_pair(MAJOR,"Major"));
	pieceNames.insert(std::make_pair(CAPTAIN,"Captain"));
	pieceNames.insert(std::make_pair(LIEUTENANT,"Lieutenant"));
	pieceNames.insert(std::make_pair(SERGEANT,"Sergeant"));
	pieceNames.insert(std::make_pair(SAPPER,"Sapper"));
	pieceNames.insert(std::make_pair(SCOUT,"Scout"));
	pieceNames.insert(std::make_pair(SPY,"Spy"));
	pieceNames.insert(std::make_pair(FLAG,"Flag"));

	//get input for the player setup
	RedP = new Player(0,true);
	BlueP = new Player(1,false);
	curPlayer = RedP;
	ifstream p1Setup(argv[1]);
	ifstream p2Setup(argv[2]);
	setUpplayer(RED,p1Setup);
	setUpplayer(BLUE,p2Setup);
	isGameover = false;
	menu();
}

Game :: Game(char ** argv){
	init(argv);
}

Game :: ~Game(){
    // Delete players and null tiles
	for(int rank = 9; rank > -1; rank--){
		for(int file = 0; file < 10; file++){
			delete board[rank][file];
			board[rank][file] = NULL;
		}
	}
    delete &RedP;
	delete &BlueP;
}

void Game :: displayBoard(){
	int lettering;
	lettering = int('J');
	cout << endl;
	for(int rank = 9; rank > -1; rank--){
		cout << "         #" << char(lettering--) << "# ";
		for(int file = 0; file < 10; file++){
			board[rank][file]->display();
			cout << " ";
		}
		cout << endl;
	}
	cout << "            ";
	for(int i = 1; i < 11; i+=1){
		cout <<"  " << i << " ";
	}
	cout << endl << endl;
	}
void Game :: setUpplayer(int colour, ifstream &pSetup){
	//Set up players
	Piece *aP;
	Tile *aT;
	int rank;
	int file;
	std::string ln;
	//RED
	if(colour == 0){
		rank = 3;
		while(getline(pSetup,ln)){
			stringstream lnstrm(ln);
			char value;
			int file = 0;
			while(lnstrm >> value){
				aT = board[rank][file];
				aP = new Piece(colour, int(value));
				aP->isVisible = true;
				aT->setPiece(aP);
				aP->setLocation(aT) ;
				file++;
			}
			rank--;
		}
	}else{
	//BLUE
		rank = 6;
		while(getline(pSetup,ln)){
			stringstream lnstrm(ln);
			char value;
			file = 9;
			while(lnstrm >> value){
				aT = board[rank][file];
				aP = new Piece(colour, int(value));
				aP->isVisible = false;
				aT->setPiece(aP);
				aP->setLocation(aT);
				file--;
			}
			rank++;
		}
	}
	//setup  water tiles
	board[5][2]->isWater = true;
	board[5][3]->isWater = true;
	board[4][2]->isWater = true;
	board[4][3]->isWater = true;
	board[5][6]->isWater = true;
	board[5][7]->isWater = true;
	board[4][6]->isWater = true;
	board[4][7]->isWater = true;
	}

void Game :: menu(){
	//Menu
	cout<< "         "<<"\033[1;44m"<<"###############################" << "\033[0m"<< endl
		<< "         "<<"\033[1;44m"<< "###############################" << "\033[0m"<< endl
		<< "         "<<"\033[1;44m"<<"###############################" << "\033[0m"<< endl
		<< "         "<<"\033[1;44m"<<"###############################" << "\033[0m"<< endl
	    << "         " << "####### S T R A T E G O #######" << endl
	    << "         "<<"\033[1;41m"<<"###############################" << "\033[0m"<< endl
	    << "         "<<"\033[1;41m"<< "###############################" << "\033[0m"<< endl
	    << "         "<<"\033[1;41m"<< "###############################" << "\033[0m"<< endl
	    << "         "<<"\033[1;41m"<< "###############################" << "\033[0m" << endl
	    << "          A command-line iteration of the game stratego.  "  << endl
	    << "          '1' to play stratego.             "  << endl
	    << "          any other key to exit.                          "  << endl;
	
	int inp;
		 cin >> inp;
		 switch(inp){
		 case 1:
		 play();
		// break;
		 case 2:
		
		 default:
		 
		 break;
		 }
		 
		
	
	}
bool Game :: isMoveValid(int sr,int sf,int tr,int tf,Player* cur){
	//Off the board
	if(sr < 0 || sr > 9 || sf < 0 || sf > 9 || tr < 0 || tr > 9 || tf < 0 || tf > 9){
		cout << "Choose locations on the board!" << endl;
		return false;
	}
	//end location is water in the middle of the board
	if(board[tr][tf]->isWater){
		cout << "Can not move piece into water!" << endl;
		return false;
	}
	// no piece picked
	if(sr == tr && sf == tf){
		cout << "Can not move piece to its current location!" << endl;
		return false;
	}
	if(!board[sr][sf]->isOccupied()){
		cout << "Do not pick an empty start location!" << endl;
		return false;
	}
	//Opponent's piece
	if(cur->getColour() != board[sr][sf]->getOccupant()->getColour()){
		cout << "Pick your own piece!" << endl;
		return false;
	}
	//movement restrictions
	//scouts can move any distance in one of the four directions
	// every other movable piece can only move one square in any of the four directions
	int deltaRank = tr - sr;
	int deltaFile = tf - sf;
	//cout << "DELTA RANK AND FILE" << deltaRank<< " " << deltaFile<< endl;
	if(deltaRank != 0 && deltaFile != 0){
		cout << "Can not move Pieces diagonally!" << endl;
		return false;
	}
	if(board[sr][sf]->getOccupant()->type == SCOUT){
		if(deltaRank == 0){//file changes
			for(int i = 1; i < deltaFile; ++i){
				if(board[sr][sf+i]->isWater){
					cout << "Can not cross water!" << endl;
					return false;
				}
				if(board[sr][sf+i]->isOccupied()){
					cout << "Can not jump over pieces! file wise" << endl;
					return false;
				}
			}
		}else{//rank changes
			for(int i = 1; i < deltaRank; ++i){
				if(board[sr+i][sf]->isWater){
					cout << "Can not cross water!" << endl;
					return false;
				}
				if(board[sr+i][sf]->isOccupied()){
					cout << "Can not jump over pieces! rank wise" << endl;
					return false;
				}
			}

		}
	}else{
		if(abs(deltaRank) == 1 && deltaFile == 0){
			return true;
		}else if(abs(deltaFile) == 1 && deltaRank == 0){
			return true;
		}else{
			cout << "Can not move your piece that far!" << endl;
			return false;
		}

	}
	//Empty space that is not a water tile or physically invalid
	if(!board[tr][tf]->isOccupied()){
		return true;
	}
	//End Location Friendly piece
	if(board[tr][tf]->getOccupant()->getColour() == cur->getColour()){
		cout << "Can not capture friendly piece!" << endl;
		return false;
	}
	//if piece is immovable
	if(board[sr][sf]->getOccupant()->type == BOMB || board[sr][sf]->getOccupant()->type == FLAG){
		cout << "That piece can not be moved!" << endl;
		return false;
	}
	return true;
}
void Game :: combat(int sr,int sf,int tr,int tf){
	string attkClr = "";
	string defnClr = "";
	if(board[sr][sf]->getOccupant()->getColour() == RED){
		attkClr = "Red";
		defnClr = "Blue";
	}else{
		attkClr = "Blue";
		defnClr = "Red";
	}
	int attkrType = board[sr][sf]->getOccupant()->type;
	int dfndrType = board[tr][tf]->getOccupant()->type;

	if(dfndrType == FLAG){
		isGameover = true;
		cout << attkClr << " captured the " << defnClr << "flag!" << endl;
		cout << "Game is over!" << endl;
		return;
	}
	if(dfndrType == BOMB){// attacking a bomb is only victory for the sapper
		if(attkrType == SAPPER){
			//Sapper wins
			board[tr][tf]->setPiece(board[sr][sf]->getOccupant());
			board[sr][sf]->setPiece(NULL);
			board[tr][tf]->getOccupant()->isVisible = true;
			cout << attkClr << " " << pieceNames[attkrType] <<" defused the  "<< defnClr << " " << pieceNames[dfndrType] << "!"  << endl;
			return;		
		}else{
			//Bomb wins 
			board[sr][sf]->setPiece(NULL);
			board[tr][tf]->getOccupant()->isVisible = true;
			cout << defnClr << " " << pieceNames[dfndrType] <<" blew up the " << attkClr << " " << pieceNames[attkrType]<< "!" << endl;
			return;
		}
	}
	if(attkrType == SPY && dfndrType == MARSHAL){
		board[tr][tf]->setPiece(board[sr][sf]->getOccupant());
		board[sr][sf]->setPiece(NULL);
		board[tr][tf]->getOccupant()->isVisible = true;
		cout << attkClr << " " << pieceNames[attkrType] <<" assassinated the  "<< defnClr << " " << pieceNames[dfndrType]<< "!" << endl;
	}
	//if power is equivalent they both get taken off board
	if(attkrType == dfndrType){
		cout << "Both a " << attkClr << " " << pieceNames[attkrType] << " and a "<< defnClr<< " " << pieceNames[dfndrType] << " killed each other!" <<endl;
		board[sr][sf]->setPiece(NULL);
		board[tr][tf]->setPiece(NULL);
		return;
	}
	if(dfndrType == SPY){
		board[tr][tf]->setPiece(NULL);
		board[tr][tf]->setPiece(board[sr][sf]->getOccupant());
		board[sr][sf]->setPiece(NULL);
		cout << defnClr << " " << pieceNames[dfndrType] << "was destroyed by the "<< attkClr <<" " <<pieceNames[attkrType] << "!" << endl;
		board[tr][tf]->getOccupant()->isVisible = true;
		return;
	}
	if(attkrType < dfndrType){
		board[tr][tf]->setPiece(NULL);
		board[tr][tf]->setPiece(board[sr][sf]->getOccupant());
		board[sr][sf]->setPiece(NULL);
		board[tr][tf]->getOccupant()->isVisible = true;
		cout << attkClr << " " << pieceNames[attkrType] << " killed a " <<defnClr << " "<<pieceNames[dfndrType] <<"!" <<  endl;
		return;
	}else{
		board[sr][sf]->setPiece(NULL);
		board[tr][tf]->getOccupant()->isVisible = true;
		cout << attkClr << " " << pieceNames[attkrType] << " was fended of by a " <<defnClr << " "<<pieceNames[dfndrType] <<"!" <<  endl;
		return;
	}
}
void Game :: play(){
		while(!isGameover){
			mov:
			if(curPlayer == RedP){// Human
				displayBoard();
				cout << "Please enter move in format: [srcrank][srcfile][trgrank][trgfile]" << endl;
				int srcfile,trgfile;
				char srcrank, trgrank;
				cin >> srcrank >> srcfile >> trgrank >> trgfile;
				srcfile-=1;
				trgfile-=1;

				int srcrankind, trgrankind;
				srcrankind = srcrank - 'a';
				trgrankind = trgrank - 'a';
				//Check to see of move is valid
				if(isMoveValid(srcrankind,srcfile,trgrankind,trgfile,curPlayer)){
					//have to actually make move and perform game state calculations
					//if space is empty proceed to make move
					if(!board[trgrankind][trgfile]->isOccupied()){
						board[trgrankind][trgfile]->setPiece(  board[srcrankind][srcfile]->getOccupant());
						board[srcrankind][srcfile]->setPiece(NULL);
					}else if(board[trgrankind][trgfile]->isOccupied()){ // if end location is occupied AND and enemy Piece
						//Perform combat caluclations based on type of piece.
						combat(srcrankind,srcfile,trgrankind,trgfile);

					}
				}else{
					//remake move
					cout << "MAKE A VALID MOVE!" << endl;
					goto mov;
				}
				
				


			}else{// AI
				//genNextMove();
				//cout << "AI Plays" << endl;
			}



			curPlayer = curPlayer == RedP ? BlueP: RedP;
		}
		//GAME is over
		cout << "Thank you for playing!" << endl;
		return;	
		}
