#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <string>
//#include "piece.h"
#include <vector>

class Player{
public:
int colour;
bool isHuman;
int wins = 0;


	Player(int colour,bool isHuman);
	~Player();
	bool makeMove();
	int getColour();
	
};
#endif
